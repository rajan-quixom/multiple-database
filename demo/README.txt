python manage.py makemigrations demoapp
python manage.py migrate demoapp --database=dataDB

dataDB is lable of the database define in settings

Patch for Forignkey:
related.py

old	new	class ForeignKey(RelatedField, Field): 
836	836	    def validate(self, value, model_instance):
837	837	        if self.rel.parent_link:
838	838	            return
839	839	        super(ForeignKey, self).validate(value, model_instance)
840	840	        if value is None:
841	841	            return
842	842	
=========================================================

843	 	        using = router.db_for_read(model_instance.__class__, instance=model_instance)
 	843	        using = router.db_for_read(self.rel.to, instance=model_instance)

=============================================================

844	844	        qs = self.rel.to._default_manager.using(using).filter(
845	845	                **{self.rel.field_name: value}
846	846	             )
847	847	        qs = qs.complex_filter(self.rel.limit_choices_to)
848	848	        if not qs.exists():
849	849	            raise exceptions.ValidationError(self.error_messages['invalid'] % {
850	850	                'model': self.rel.to._meta.verbose_name, 'pk': value})